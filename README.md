# amip

## 项目简介 ##

 - 脚本名称: amip.sh

 - 名称解释：auto mail ip

 - Copyright (C) 2022 feng <feng@archlinux>

 - Distributed under terms of the MIT license.


## 注意事项 ##

注意，局域网内只不需要配置外部邮箱。如果需要外网通知，请取消`--account=163` 一行的注释，同时设置好自己的邮箱即可。

此脚本需要使用crontab设置定时任务，同时也需要设置成开机启动程序。
