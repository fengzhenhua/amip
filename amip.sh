#! /bin/sh
#
# 脚本名称: amip.sh
# 名称解释：auto mail ip
# Copyright (C) 2022 feng <feng@archlinux>
#
# Distributed under terms of the MIT license.
#
IPIfo=$(ip addr |grep inet |grep -v inet6 |grep -v '127.0.0.1')
TheIP=$(echo $IPIfo |awk '{print $2}' |awk -F '[/]' '{print $1}')
TheName=$(hostnamectl hostname)
EmailToList=(
   "fengzhenhua@mail.bnu.edu.cn"
   "202121470018@mail.bnu.edu.cn"
   "zhilongzhao@mail.bnu.edu.cn"
)
OldIP="172.23.28.228"
if [ ! $OldIP == $TheIP ]; then
   sed -i "s/^OldIP.*$/OldIP=\"${TheIP}\"/g"  $0
   for emt in ${EmailToList[@]}; do
#      echo "$(date) 主机"$TheName"的IP发生变动，新IP为: $TheIP" | mail --account=163 -s ""$TheName"主机IP变动通知" $emt
      echo "$(date) 主机"$TheName"的IP发生变动，新IP为: $TheIP" | mail -s ""$TheName"主机IP变动通知" $emt
   done
fi

## 注意，局域网内只不需要配置外部邮箱。如果需要外网通知，请取消`--account=163` 一行的注释，同时设置好自己的邮箱即可。
